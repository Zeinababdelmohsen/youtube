/* eslint-disable no-shadow */
import { API_URL, SECRET_KEY } from '../../endpoint';

// initial state
const state = {
	items: [],
	filterType: 'all',
	totalResult: null,
	maxResult: 20,
	error: null,
};

// mutations
const mutations = {
	GET_ITEMS(state, videos) {
		const iniState = state;
		iniState.items = videos;
		return iniState;
	},
	GET_FILTER_TYPE(state, type) {
		const iniState = state;
		iniState.filterType = type;
		return iniState;
	},
	GET_TOTAL_RESULTS(state, result) {
		const iniState = state;
		iniState.totalResult = result;
		return iniState;
	},
	GET_ERROR(state, error) {
		const related = state;
		related.error = error;
		return related;
	},
};

// actions
const actions = {
	getItems({ commit }, payload) {
		if (!payload.key) return;
		let queries = `?part=snippet&q=${payload.key}&maxResults=${state.maxResult}&key=${SECRET_KEY}`;
		if (payload.type !== 'all' && payload.type !== null) {
			queries += `&type=${payload.type}`;
		}
		if (payload.sort) {
			queries += `&order=${payload.sort}`;
		}
		fetch(`${API_URL}/search${queries}`)
			.then((res) => {
				if (res.status === 200) {
					res.json().then((data) => {
						if (data) {
							commit('GET_ITEMS', data.items);
							commit('GET_TOTAL_RESULTS', data.pageInfo.totalResults);
						}
					});
				} else {
					res.json().then((err) => {
						commit('GET_ERROR', err.error.message);
					});
				}
				payload.router.push({ name: 'Search', query: { text: payload.key } });
			})
			.catch((error) => {
				console.log(error);
			});
	},
	getFilterType({ commit }, type) {
		commit('GET_FILTER_TYPE', type);
	},
};

export default {
	namespaced: true,
	state,
	actions,
	mutations,
};
