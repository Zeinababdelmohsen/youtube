/* eslint-disable no-shadow */
import { API_URL, SECRET_KEY } from '../../endpoint';

// initial state
const state = {
	videoDetails: null,
	relatedVideos: null,
	playlistItems: null,
	maxResult: 20,
	error: null,
};

// mutations
const mutations = {
	GET_VIDEO(state, video) {
		const details = state;
		details.videoDetails = video;
		return details;
	},
	GET_RELATED_VIDEOS(state, videos) {
		const related = state;
		related.relatedVideos = videos;
		return related;
	},
	GET_PLAYLIST_ITEMS(state, videos) {
		const related = state;
		related.playlistItems = videos;
		return related;
	},
	GET_ERROR(state, error) {
		const related = state;
		related.error = error;
		return related;
	},
};

// actions
const actions = {
	getVideoDetails({ commit }, videoID) {
		fetch(`${API_URL}/videos?id=${videoID}&part=snippet,statistics,player&key=${SECRET_KEY}`)
			.then((res) => {
				if (res.status === 200) {
					res.json().then((data) => {
						commit('GET_VIDEO', ...data.items);
					});
				} else {
					res.json().then((err) => {
						commit('GET_ERROR', err.error.message);
					});
				}
			}).catch(() => {});
	},
	getRelatedVideos({ commit }, relatedVideoID) {
		fetch(`${API_URL}/search?part=snippet&relatedToVideoId=${relatedVideoID}&type=video&key=${SECRET_KEY}&maxResults=${state.maxResult}`)
			.then((res) => res.json())
			.then((data) => {
				commit('GET_RELATED_VIDEOS', data.items);
			});
	},
	getPlaylistItems({ commit }, playlistID) {
		fetch(`${API_URL}/playlistItems?part=snippet,contentDetails&playlistId=${playlistID}&key=${SECRET_KEY}&maxResults=${state.maxResult}`)
			.then((res) => {
				if (res.status === 200) {
					res.json().then((data) => {
						commit('GET_PLAYLIST_ITEMS', data.items);
					});
				} else {
					res.json().then((err) => {
						commit('GET_ERROR', err.error.message);
					});
				}
			})
			.catch(() => {});
	},
};

export default {
	state,
	actions,
	mutations,
};
