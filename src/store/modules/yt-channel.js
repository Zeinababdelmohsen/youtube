/* eslint-disable no-shadow */
import { API_URL, SECRET_KEY } from '../../endpoint';
// initial state
const state = {
	channelDetails: null,
	relatedPlaylists: null,
	error: null,
};

// mutations
const mutations = {
	GET_CHANNEL(state, channel) {
		const details = state;
		details.channelDetails = channel;
		return details;
	},
	GET_RELATED_PLAYLISTS(state, channels) {
		const related = state;
		related.relatedPlaylists = channels;
		return related;
	},
	GET_ERROR(state, error) {
		const related = state;
		related.error = error;
		return related;
	},
};

// actions
const actions = {
	getChannelDetails({ commit }, channelID) {
		fetch(`${API_URL}/channels?part=snippet,statistics&id=${channelID}&key=${SECRET_KEY}`)
			.then((res) => {
				if (res.status === 200) {
					res.json().then((data) => {
						commit('GET_CHANNEL', ...data.items);
					});
				} else {
					res.json().then((err) => {
						commit('GET_ERROR', err.error.message);
					});
				}
			})
			.catch(() => {});
	},
	getRelatedPlaylists({ commit }, relatedChannelID) {
		fetch(`${API_URL}/playlists?part=snippet,contentDetails&channelId=${relatedChannelID}&key=${SECRET_KEY}`)
			.then((res) => res.json())
			.then((data) => {
				commit('GET_RELATED_PLAYLISTS', data.items);
			});
	},
};

export default {
	state,
	actions,
	mutations,
};
