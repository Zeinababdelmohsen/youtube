import { createStore } from 'vuex';
import videos from './modules/yt-videos';
import searchItems from './modules/yt-search';
import channel from './modules/yt-channel';

const store = createStore({
	modules: {
		videos,
		searchItems,
		channel,
	},
});
export default store;
