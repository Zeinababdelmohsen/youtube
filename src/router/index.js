import { createRouter, createWebHistory } from 'vue-router';
import Home from '../pages/Home.vue';
import Video from '../pages/Video.vue';
import Channel from '../pages/Channel.vue';
import Search from '../pages/Search.vue';
import NotFound from '../pages/NotFound.vue';
import Playlist from '../pages/Playlist.vue';

const routes = [
	{ path: '/', component: Home, name: 'Home' },
	{ path: '/search', component: Search, name: 'Search' },
	{ path: '/video/:id', component: Video, name: 'Video' },
	{ path: '/channel/:id', component: Channel, name: 'Channel' },
	{ path: '/playlist/:id', component: Playlist, name: 'Playlist' },
	{ path: '/:catchAll(.*)', component: NotFound, name: 'NotFound' },
];

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
});

export default router;
