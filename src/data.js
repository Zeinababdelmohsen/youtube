export const typeOptions = [
	{ text: 'All', value: 'all' },
	{ text: 'Video', value: 'video' },
	{ text: 'Channel', value: 'channel' },
	{ text: 'Playlist', value: 'playlist' },
];

export const sortByOptions = [
	{ text: 'Relevance', value: 'relevance' },
	{ text: 'Upload date', value: 'date' },
	{ text: 'View count', value: 'viewCount' },
	{ text: 'Rating', value: 'rating' },
];
