const formatDate = (date) => {
	let formattedDate = null;
	formattedDate = new Date(date).toLocaleDateString();
	return formattedDate;
};

export default formatDate;
