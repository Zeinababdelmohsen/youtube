# Youtube app

Is an application for viewing videos, channels, and playlists.
The user can search with any keyword.
User can filter by **Type** such as `video`, `channel` and `playlist`. Or **Sort By** `relevance`, `upload data`, `view count` and `rating`.

## Features

- Search page.
- Details page displayed based on the selected `video` or `playlist`.
- Channel page for selected channel.
- Filtering with **Type**, **Sort By**.

## Development Usage

This project was built with [Vue v.3](https://v3.vuejs.org/), [Vuex v.4](https://next.vuex.vuejs.org/) as a **state management** and [SASS](https://sass-lang.com/) as a **CSS** preprocessor.

- Install the latest LTS version of Node.js from https://nodejs.org.
- Clone the project on your machine and then create a `.env` file for environment variables, see the `.env.dist` as an example.
- Install [Yarn](https://yarnpkg.com/) and then type `yarn install` in the terminal from project root to install the dependencies as defined in the package.json file.
- Then run `yarn serve` it will run and then navigate to `http://localhost:8080/` server.

## Architecture

- `src/` holds all screens, components, services and routing for the app.

- `/assets` holds all **SASS** files, **Images** and **Fonts** used throughout the app.

- `/components` holds all public components for the app such as `header` , `loader` and `blocks`.

- `/pages` holds all app screens; **Home**, **Search**, **Video**, **Playlist** and **Channel**.

- `/router` contains `index.js` that holds all the routes of the app.

- `/store` contains all state modules used throughout the app.

## Linting

- [Eslint](https://eslint.org/)

Run this commannd to detect the warnings and the errors
```
yarn lint
```
For fixing eslint errors & warnings run the command below: 
```
yarn lint:fix
```
